# Le rendu

<!-- .slide: class="slide" -->

---

## Le code

<!-- .slide: class="slide" -->

- Un code fonctionnel
- Un code indenté et lisible
- Un code documenté

--> Tout votre code doit également être disponible sur git

---

## Le Rapport

<!-- .slide: class="slide" -->

- Un rapport de 15 pages (intro / mise en contexte + developpement + conclusion)
- Des diagrammes UML: Cas utilisation + Diagramme de classe correspondant a votre code.
- Des explications sur des choses qui vous simple importante (justification de votre code)
- ce qui a marché ce qui a pas marché.

---

## La soutenance

<!-- .slide: class="slide" -->

- Pas de gros pavé de code sauf si y a un vrai interet
- Organisation de l'équipe
- présenter votre projet, comment il marche ?
- Démo
- question
