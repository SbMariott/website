# Préparation de votre poste

<!-- .slide: class="slide" -->

---

## Ce qu'on a besoin :

<!-- .slide: class="slide" -->

- Un IDE (environnement de developpement) : Vscode ou Pycharm (perso je prefere vscode)
<!-- .element: class="fragment" -->
- un client git
<!-- .element: class="fragment" -->
- python
<!-- .element: class="fragment" -->

---

## Installation python (Windows)

<!-- .slide: class="slide" -->

- https://www.python.org/downloads/ --> télécharger la dernière version
- Bien cliquer Add Python 3.x.y to PATH (Ajouter Python 3.... à PATH).
- Une fois installation terminée ouvrir un terminal (cmd ou powershell) et taper :

```bash
C:\Windows\System32> python --version
Python 3.x.y
```

---

## Installation python (MacOs)

<!-- .slide: class="slide" -->

- https://www.python.org/downloads/ --> télécharger la dernière version
- Bien cliquer Add Python 3.x.y to PATH (Ajouter Python 3.... à PATH).
- Une fois installation terminée ouvrir un terminal et taper :

```bash
C:\Windows\System32> python --version
Python 3.x.y
```

---

## Installation vscode (MacOS et Windows)

<!-- .slide: class="slide" -->

- ici : [https://code.visualstudio.com/download](https://code.visualstudio.com/download)

---

## Installation pycharm

- Ici : [https://www.jetbrains.com/fr-fr/community/education/#students](https://www.jetbrains.com/fr-fr/community/education/#students)
- Vous etes étudiant vous pouvez avoir une licence pycharm avec plein de chose en plus.
- Mettre votre adresse ENSAI

---

## Installation git

<!-- .slide: class="slide" -->

- installation de gitBash (Windows): [https://gitforwindows.org/](https://gitforwindows.org/)
- installation git-scm (MacOS): [https://sourceforge.net/projects/git-osx-installer/](https://sourceforge.net/projects/git-osx-installer/)
- Ouvrir terminal et taper git, un texte semblable devrait s'afficher :

```bash
test ~/random/path > git
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

These are common Git commands used in various situations:

start a working area (see also: git help tutorial)
   clone             Clone a repository into a new directory
   init              Create an empty Git repository or reinitialize an existing one

work on the current change (see also: git help everyday)
   add               Add file contents to the index
   mv                Move or rename a file, a directory, or a symlink
   restore           Restore working tree files
   rm                Remove files from the working tree and from the index
   sparse-checkout   Initialize and modify the sparse-checkout
```

---

## Configurer git

- Pour pouvoir envoyer du code faire une fois

```
git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```

---
