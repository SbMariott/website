# Configuration du poste

<!-- .slide: class="slide" -->

---

## Installation vscode (MacOS et Windows)

<!-- .slide: class="slide" -->

- ici : [https://code.visualstudio.com/download](https://code.visualstudio.com/download)

---

## Installation git

<!-- .slide: class="slide" -->

- installation de gitBash (Windows): [https://gitforwindows.org/](https://gitforwindows.org/)
- installation git-scm (MacOS): [https://sourceforge.net/projects/git-osx-installer/](https://sourceforge.net/projects/git-osx-installer/)
- installation git (linux): `sudo apt install git-all`
- Ouvrir terminal et taper git, un texte semblable devrait s'afficher :

```bash
test ~/random/path > git
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

These are common Git commands used in various situations:

start a working area (see also: git help tutorial)
   clone             Clone a repository into a new directory
   init              Create an empty Git repository or reinitialize an existing one

work on the current change (see also: git help everyday)
   add               Add file contents to the index
   mv                Move or rename a file, a directory, or a symlink
   restore           Restore working tree files
   rm                Remove files from the working tree and from the index
   sparse-checkout   Initialize and modify the sparse-checkout
```

---

## Configurer git

- Configuration de git

```
git config --global user.name "Prénom Nom"
$ git config --global user.email yourEmail@email.com
```

---

## Création de vos clés ssh (Sur votre poste)

<!-- .slide: class="slide" -->

Objectif : s'authentifier auprés de gitlab

- ⚠️ Toute cette manip ne fonctionnera que sur votre poste courant il faudra la refaire si vous changez de poste
- Ouvrir terminal (MacOs) ou GitBash (Windows)
- `ssh-keygen -t rsa -b 4096 -C "VotreAdresseEmail@example.com"`
- puis appuyez entrer jusqu'a la fin !!!
- `cat $HOME/.ssh/id_rsa.pub` --> on cherche ce fichier et on copie ce qu'il y a dedans

---

## Ajout de votre nouvelle clé ssh (Sur gitlab)

<!-- .slide: class="slide" -->

- Se connecter et cliquer sur votre avatar en haut a droite
- Cliquer Settings (ou preférences)
- Sur le coté cliquer SSH Keys (clefs SSH)
- Dans le grand carré coller ce que vous venez de copier
- Cliquer sur ajouter une clef
