# Gitlab

<!-- .slide: class="slide" -->

---

<!-- .slide: class="slide" -->

- Concurrent direct à Github
- Open source (_un peu_)
- Self hosted
- une api et une documentation **complète**

---

## Principales fonctionnalités

<!-- .slide: class="slide" -->

Création de projet appartenant à un utilisateur (ou un groupe), contenant :

- Git
- Tracker
- Wiki
- Docker registry
- Pages
- WebIDE
- **CI/CD**

[control Repo](https://gitlab.insee.fr/ystfg5/control-repo-horsprod)

---

## Git

<!-- .slide: class="slide" -->

- Dépot git remote
- Jolie interface
- Vue des commits / branches
- Traitements des tags à part

---

## Git

<!-- .slide: class="slide" -->

![repo](assets/repo.png)

---

## Géré les droits sur un dépot

- guest: peut uniquement parcourir les issues, le code et MR
- reporter: peut commenter dans les pr mais ne peut pas publier de code
- developper: peut ajouter du code mais uniquement par merge request qui seront validée par un maintener
- maintener: est responsable du dépot => peut tout faire sur le dépot (code)
- owner: propriétaire du dépot = celui qui l'a créé (peut ajouter des droits)

---

## Tracker

<!-- .slide: class="slide" -->

- Gestion des "issues"
- Labels
- Tableau (Kanban)
- Markdown + lien au dépôt git

---

## Tracker

<!-- .slide: class="slide" -->

![tracker](assets/tracker.png)

---

## Wiki

<!-- .slide: class="slide" -->

- classique
- `nomduprojet.wiki`
- markdown

---

## Docker registry

<!-- .slide: class="slide" -->

- Héberge les images docker
- Avec les permissions du projet (par défaut)
- Lien direct avec gitlab CI

---

## Pages

<!-- .slide: class="slide" -->

- Démon permettant de servir des sites statiques
- (= github.io)
- `this`

---

## WebIDE

<!-- .slide: class="slide" -->

- Pour certaines personnes qui refusent d'utiliser leur poste de travail
- coloration syntaxique
- complétion (un peu)

---

## Intégration Continue/ Livraison Continue

<!-- .slide: class="slide" -->

![cicd_pipeline_infograph](assets/cicd_pipeline_infograph.png)

---

## Principe

<!-- .slide: class="slide" -->

- Un service `runner` s'enregistre auprès de gitlab
- enregistrement par instance/groupe/projet
- A chaque évènement, gitlab essaie de trouver un runner pour prendre en charge le job
- jobs déclarés dans un fichier `.gitlab-ci.yml`

---

## Types de runners

<!-- .slide: class="slide" -->

- npm: les jobs s'executent dans un environnement node
- java: les jobs s'executent dans un environement java
- docker: on peut choisir une image docker qui va executer le job

---

## `gitlab-ci.yml`

<!-- .slide: class="slide" -->

Voir la doc
➡️ https://docs.gitlab.com/ce/ci/

---

## On mixe le tout

<!-- .slide: class="slide" -->
<img src="assets/mixer.gif" width="600"/>

---

## Une issue est déclarée sur le tracker

<!-- .slide: class="slide" -->

https://git.stable.innovation.insee.eu/outils-transverses/contacts-externes/ldap-services/issues/45

![issue](assets/issue.PNG)

Note:
https://git.stable.innovation.insee.eu/outils-transverses/contacts-externes/ldap-services/issues/45
https://git.stable.innovation.insee.eu/outils-transverses/contacts-externes/ldap-services/issues/46

---

## Création d'une branche pour corriger

<!-- .slide: class="slide" -->

![create-branch from issue](assets/create-branch%20from%20issue.PNG)

---

## Corrections

<!-- .slide: class="slide" -->

![cat](assets/cat.webp)

Note:
Pour 46: https://git.stable.innovation.insee.eu/snippets/51

---

## On commit notre code

<!-- .slide: class="slide" -->

> on perd pas de temps à tester

https://git.stable.innovation.insee.eu/outils-transverses/contacts-externes/ldap-services/commit/4d26267f5da4560c4437dce1fc519d2ba9545fb7

![commit](assets/commit.PNG)

---

## On fait la merge request

<!-- .slide: class="slide" -->

https://git.stable.innovation.insee.eu/outils-transverses/contacts-externes/ldap-services/merge_requests/40

![mr](assets/mr.PNG)

---

## Et on attend que le pipeline passe

<!-- .slide: class="slide" -->

![source](assets/source.gif)

---

## Une fois le pipeline passé

<!-- .slide: class="slide" -->

![pipeline](assets/pipeline.PNG)

https://git.stable.innovation.insee.eu/outils-transverses/contacts-externes/ldap-services/pipelines/14465

-> Le pipeline me dit si mon code est correcte

---

## Merge request acceptée

<!-- .slide: class="slide" -->

- Issue est fermée avec un commentaire
- La branche git est supprimée
- Le code est ajouté a la branche principale puis un nouveau pipeline prends le relai, il déploiera les changements sur l'environnemment en question.

---

## Avantage des merge requests

- Suivi des tâches
- Revert facilité
