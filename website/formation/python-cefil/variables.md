# Variables et fonctions

<!-- .slide: class="slide" -->

---

## C'est quoi une variable ?

<!-- .slide: class="slide" -->

- Un objet qui contient des informations
- Un moyen d'enregistrer des données
- Une variable a un type (entier, string, liste, tableau,...)

🚨 Contrairement à d'autres langages de programmation, Python est dit _dynamiquement_ typé : il est possible de réassigner une variable à un objet de type différent. Cela facilite la lecture et le développement, mais peut parfois générer des problèmes difficiles à débugger...

⚠️ Il faut donc toujours bien faire attention que le type de la variable est bien celui que l'on s'imagine manipuler.

---

## Comment créer une variable

<!-- .slide: class="slide" -->

```python
variable1 = "Ma premiere variable"
variable2 = 2

```

📝 Quand on dit déclarer (ou initialiser) une variable, c’est juste une autre façon de dire « créer une variable ».

---

## Les Différents Types :

<!-- .slide: class="slide" -->

**Nombre** : entier, float

**String** : chaine de caractères pouvant contenir n'importe quel caractère ou ensemble de caractères. C'est une séquence

**Les conteneurs** : les séquences (les chaines, les listes, les tuples), les tableaux associatifs (les dictionnaires), les fichiers,...

```python
variable1 = "Ma premiere variable"
```

⚠️ variable1 est à la fois de type string et de type séquence.

---

## Les listes

<!-- .slide: class="slide" -->

**Définition** : Une liste est une collection modifiable d’éléments éventuellement
hétérogènes.

**Syntaxe** : Éléments séparés par des virgules, et entourés de crochets : "[]".
Contiennent des élements. Chaque élément peut être de n’importe quel type (
entier, réel, chaine de caractères, liste ...).
De taille quelconque, peut grandir, rétrécir, être modifiée, être encapsulée (liste de
listes de listes)

```python
l = [1,2,3,4]
print(l)
>> [1, 2, 3, 4]
liste = [1,2,['a', 'b'],'bonjour ']
print(liste)
>> [1, 2, ['a', 'b'], 'bonjour ']
```

---

## Les tuples

<!-- .slide: class="slide" -->

**Définition** : Un tuple est une collection non modifiable d’éléments éventuellement
hétérogènes.

**Syntaxe** : Éléments séparés par des virgules, et entourés de parenthèses (..., ..., ...).
utiles pour définir des constantes.
Comme des listes, sauf qu’ils ne peuvent pas être modifiés une fois créés,
c’est-à-dire qu’ils sont immuables

---

## Les dictionnaires

<!-- .slide: class="slide" -->

Un dictionnaire est un type de données permettant de stocker des couples (clé : valeur), avec un accès très rapide à la valeur à partir de la clé, la clé ne pouvant être présente qu’une seule fois dans le tableau. Il possède les caractéristiques suivantes :

- l’opérateur d’appartenance d’une clé (in) ;
- la fonction taille (len()) donnant le nombre de couples stockés ;
- il est itérable (on peut le parcourir).

Les dictionnaires sont également des listes, sauf que chaque élément est une paire clé-valeur.

- la syntaxe des dictionnaires est key1: value1, ...:
- chaque clé est unique

---

## Exemple

<!-- .slide: class="slide" -->

```python
mois ={ 'Jan ':31 , 'Fev ':28 , 'Mar ':31}
print(mois)
>> {'Mar ': 31, 'Fev ': 28, 'Jan ': 31}

mois.keys()
>> ['Jan ', 'Fev ', 'Mar ']

mois.values()
>> [31, 28, 31]

mois.items()
>> [('Mar ', 31), ('Jan ', 31), ('Fev ', 28)]

del mois['Mar '] # supprimer une clé
print(mois)
>> {'Jan ': 31, 'Fev ': 28}

'Fev' in mois # Test de présence d'une clé
>> True
```

---

## Les conditions : if, elif, else

<!-- .slide: class="slide" -->

```python
import random ##on importe ici une bibliothèque externe
base = random.choice(["a", "t", "c", "g",])
if base == "a":
    print("choix d'une adénine")
elif base == "t":
    print("choix d'une thymine")
elif base == "c":
    print("choix d'une cytosine")
elif base == "g":
    print("choix d'une guanine")
else:
    print("ceci n'est pas possible")

```

---

## Les comparateurs

<!-- .slide: class="slide" -->

| Syntaxe Python | Signification       |
| :------------- | :------------------ |
| ==             | égal à              |
| !=             | différent de        |
| >              | supérieur à         |
| <=             | supérieur ou égal à |
| <              | inférieur à         |
| >=             | inférieur ou égal à |

---

## Les boucles

<!-- .slide: class="slide" -->

En programmation, on est souvent amené à répéter plusieurs fois une instruction. Incontournables à tout langage de programmation, les boucles vont nous aider à réaliser cette tâche de manière compacte et efficace.

Par exemple :

```python
animaux = ["girafe", "tigre", "singe", "souris"]
print(animaux[0])
print(animaux[1])
print(animaux[2])
print(animaux[3])
```

---

## Boucle For (1/2)

<!-- .slide: class="slide" -->

Itérer sur les éléments d'une liste :

```python
animaux = ["girafe", "tigre", "singe", "souris"]
for animal in animaux:
    print(animal)

-> girafe
-> tigre
-> singe
-> souris
```

On remarquera les `:` pour marquer le début de la boucle et l'indentation pour dire que l'on reste dans la boucle. Lorsque on n'indente plus, Python considère qu'on est sorti de la boucle. A l'inverse R qui utilise des `{}` et qui se moque de l'indentation.

---

### La méthode range

<!-- .slide: class="slide" -->

L'instruction `range()` est une fonction spéciale en Python qui génère des nombres entiers compris dans un intervalle.

```python
>>> list(range(10))
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

>>> list(range(15, 20))
[15, 16, 17, 18, 19]
```

---

## Boucle For (2/2)

<!-- .slide: class="slide" -->

```python
>>> for i in range(4):
...     print(i)
...
0
1
2
3
```

⚠️ On remarquera l'absence du mot clé list non nécessaire ici.

---

## Boucle While

Une autre alternative à l'instruction for couramment utilisée en informatique est la boucle while. Le principe est simple. Une série d'instructions est exécutée tant qu'une condition est vraie. Par exemple :

```python
i = 1
while i <= 4:
    print(i)
    i = i + 1

```

---

## TP1

 <!-- .slide: class="slide" -->

-> [TP1](/website/formation/python-cefil/Tps/tp1.html)
