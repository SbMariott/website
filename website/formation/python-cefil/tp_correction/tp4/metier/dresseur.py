class Dresseur:
    def __init__(self, nom: str) -> None:
        self.nom = nom
        self.pokemons = []

    def capture(self, pokemon) -> None:
        self.pokemons.append(pokemon)