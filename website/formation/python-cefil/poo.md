# Programmation orientée objet

<!-- .slide: class="slide" -->

---

## Constat (1/3)

<!-- .slide: class="slide" -->

On voudrait manipuler/représenter des données complexes :

- Une personne : son nom, son prénom, son âge, son adresse,...
- Une voiture : ses pneus (qui peuvent eux-mêmes être des objets)
- Un pokemon : son type, ses PV, son attaque, sa défense,...

---

## Constat (2/3)

<!-- .slide: class="slide" -->

On pourrait utiliser les listes :

```python
# pour representer une personne
personne = ['Dupont ','Paul', 21, 'Rue Blaise Pascal , 35172 Bruz']
# pour savoir s'il est majeur
personne[2] > 18
```

- Numérotation des caractéristiques
- Pas pratique

---

## Constat (3/3)

<!-- .slide: class="slide" -->

On pourrait utiliser des dictionnaires :

```python
personne = {'nom':'Dupont ','prenom ':'Paul', 'age':21,'adresse ':'Rue Blaise Pascal , 35172 Bruz'}
personne['age'] > 21
```

<span style="color:red">Mais : </span>

C'est lourd à manipuler

Il faut connaitre les champs du dictionnaire à l'avance

---

## Construire son propre type

<!-- .slide: class="slide" -->

Python (comme tous les langages de programmation) permet de définir son propre
type de données.

**Objectif** : Pouvoir définir et utiliser des fractions ou des personnes ainsi :

```python
p1 = Personne("Dupont" ,"Jacques", 25)
print (p1)
Jacques Dupont , 25 ans
p2 = Personne("Bernard","Xavier", 13)
print (p2)
Xavier Bernard , 31 ans
p2.estMajeur ()
False

```

🧠 un type : c'est des données et des fonctions pour manipuler les données.

---

## Construire son propre type(1/?)

<!-- .slide: class="slide" -->

Un nouveau type de données s'appellera une classe en Python.

```python
class NomDeLaClasse:
    #on définit ici LES DONNEES
    #on définit ici LES FONCTIONS
```

⚠️ Il faudra bien veiller à l'indentation pour ajouter des données ou des fonctions dans votre classe.

🧠 En Python tout est objet, de la chaine de caractères au tableau, donc finalement vous utilisez des classes depuis le début.

---

## Construire son propre type

<!-- .slide: class="slide" -->

On appelle instance un objet construit sur le modèle d’une classe.

Une classe permet de generer **n** instances d'un objet (on parle alors d'instanciation).

Pour instancier un objet à partir d'une classe il nous faut un **constructeur**. Un constructeur est une méthode spéciale. Cette méthode va prendre un certain nombre d'arguments et va permettre d'initialiser les attributs.

---

## Le constructeur

<!-- .slide: class="slide" -->

Chaque classe possède un constructeur : c’est la méthode qui permet de créer une instance. Elle s’appelle **\_\_init\_\_**:

- Comme toutes les méthodes, son premier paramètre est self : c’est la variable qui contient la référence de l’instance courante.
- Elle définit les informations qui doivent être fournies pour construire une instance (ses autres paramètres).
- C’est dans le constructeur qu’on définit et qu’on initialise les attributs des instances.

<!-- .slide: class="slide" -->

```python
class Personne:

    def __init__(self ,nom , prenom , age):
        self.nom=nom # attribut nom
        self.prenom=prenom # attribut prenom
        self.age=age # attribut age


moi=Personne("Eneman","Donatien",26)
moi.nom #¨pour acceder a l'attribut nom
```

---

# Les méthodes

<!-- .slide: class="slide" -->

Chaque classe possède des méthodes : ce sont les fonctionnalités que l’objet doit fournir :

- pour le cas des personnes, on veut qu’une instance puisse dire si elle est majeur, si elle
  habite à une adresse bien définie, ...

La classe est responsable de ses attributs, elle doit contrôler leur cohérence.

Le premier paramètre d’une méthode est toujours self.

---

## Pour résumer

<!-- .slide: class="slide" -->

```python
class Personne:

    def __init__(self ,nom , prenom , age, adresse):
        self.nom=nom # attribut nom
        self.prenom=prenom # attribut prenom
        self.age=age # attribut age
        self.adresse=adresse # attribut addresse

    def est_majeure(self):
        return self.age>18

    def habite(self,addresse):
        return self.adresse==adresse

    def demenage(self,adresse):
        self.adresse=addresse

moi=Personne("Eneman","Donatien",26,"Paris")
moi.estMajeure()
moi.habite("Bordeaux")
moi.demenage("Bordeaux")
moi
```

---

**Vocabulaire :**

<!-- .slide: class="slide" -->

- Les fonctions définies dans les classes sont appelées Méthodes
- Les données sont appelées des Attributs (ces attributs peuvent être eux-mêmes des instances d'une autre classe)
- Une instance d'une classe est un objet

---

## Pour résumer

<!-- .slide: class="slide" -->

Une classe est :

- un TYPE qui permet de créer des objets (la classe est un moule)
- une UNITÉ D’ENCAPSULATION qui regroupe :
  - attributs stockage d’information (état de l’objet).
  - méthodes unités de calcul (fonctions) dont le premier paramètre est self.
  - un ESPACE DE NOMS (namespace) : deux classes différentes peuvent avoir des membres de même nom

---

## Les 3 fondamentaux de la POO

<!-- .slide: class="slide" -->

La Programmation Orientée Objet est dirigée par trois fondamentaux qu'il convient de toujours garder à l'esprit : encapsulation, héritage et polymorphisme.

---

## Encapsulation

<!-- .slide: class="slide" -->

Encapsulation : un objet contient des attributs et des méthodes que l'on peut rendre visible à l'extérieur de l'objet ou non (en Python il n'est pas possible de gérer cette notion de visibilité elle ne sera donc pas vue) ;

---

## Héritage

<!-- .slide: class="slide" -->

Héritage : un objet peut hériter d'un autre et avoir accès à ses attributs ou méthodes comme s'ils étaient les siens ;

---

## Polymorphisme

<!-- .slide: class="slide" -->

Polymorphisme : une même méthode peut avoir un comportement différent en fonction de l'objet auquel elle appartient.

---

## TP3

 <!-- .slide: class="slide" -->

-> [TP3](/website/formation/python-cefil/Tps/tp3.html)
