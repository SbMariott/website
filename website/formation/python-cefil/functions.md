# Les fonctions

<!-- .slide: class="slide" -->

---

## C'est quoi ?

<!-- .slide: class="slide" -->

Une fonction c'est en quelque sorte une boite que vous pouvez appeler n'importe quand et :

- À laquelle vous passez aucune, une ou plusieurs variable(s) entre parenthèses. Ces variables sont appelées arguments. Il peut s'agir de n'importe quel type d'objet Python.
- Qui effectue une ou plusieurs actions
- Et qui renvoie un objet Python ou rien du tout.

![schema_fonction](assets/schema_fonction.png)

---

## Définition

<!-- .slide: class="slide" -->

Pour définir une fonction, Python utilise le mot-clé **def**. Si on souhaite que la fonction renvoie quelque chose, il faut utiliser le mot-clé return. Par exemple :

```python
>>> def carre(x):
...     return x**2
...
```

🧠 Dans l'exemple ci-dessus x est un argument, c'est à dire un paramètre de la fonction. Une fonction peut avoir n arguments. En Python le nom de la fonction + ses arguments constitue sa signature.

---

## Intérêt

Les fonctions ont pour but de:

- Découper votre code en bouts cohérents
- Simplifier la relecture du code
- Faciliter la réutilisation du code

---

## Variable locale et variable globale

```python
# définition d'une fonction carre()
def carre(x):
    x = x**2
    return y

# programme principal
x=5
resultat = carre(x)
print(x)
???
print(resultat)
???
```

-> Pour mieux comprendre [https://pythontutor.com/](https://pythontutor.com/)

---

Une variable est dite locale lorsqu'elle est créée dans une fonction. Elle n'existera et ne sera visible que lors de l'exécution de la dite fonction.

Une variable est dite globale lorsqu'elle est créée dans le programme principal. Elle sera visible partout dans le programme.

---

## TP2

-> [TP2](/website/formation/python-cefil/Tps/tp2.md)
