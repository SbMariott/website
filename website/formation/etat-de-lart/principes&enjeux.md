﻿# Principe et enjeux

<!-- .slide: class="slide" -->

---

<!-- .slide: class="slide" -->

- Le développement logiciel ne cesse de se transformer.
- Ces dernières années, les changements majeurs ont principalement concerné la façon de packager, de publier et de déployer les applications.
- Cet atelier vise à faire une revue des bonnes pratiques sur ces sujets.
- Le fil rouge utilisera une application type Insee actuelle ce qui permettra d'illustrer et d'appliquer concrètement le chemin progressif vers une modernisation forte du parc applicatif.

---

## Fil Rouge

<!-- .slide: class="slide" -->

J'ai une application insee dans son jus et je souhaite pouvoir la remettre au gout du jour. Tout le code technique sera donné le but est de se familiariser avec les concepts. [https://appli-opensource-main.dev.insee.io](https://appli-opensource-main.dev.insee.io)

Objectifs :

- Préparer une appli représentative du parc Insee pour être rendue opensource.
- Se familiariser avec les différents outils mis à disposition pour déployer une application.
- Découvrir le déploiement cloud
- Approfondir les pratiques de CI/CD

➡️ A la fin je saurai rendre une application opensource et maitriserai les bases du déploiement cloud

---

## L'application en question

<!-- .slide: class="slide" -->

Partie opensource:
➡️ [https://gitlab.com/Donatien26/appli-opensource](https://gitlab.com/Donatien26/appli-opensource)

```
- git clone https://gitlab.com/Donatien26/appli-opensource.git
- git checkout step1-clean-env-var
- mvn clean package -DskipTests

```

(vous aurez besoin d'un postgres portable)

---

## Portabilité

<!-- .slide: class="slide" -->

- Fonctionnement identique peu importe l'environnement dans lequel l'appli tourne

---

## Intéropérabilité

<!-- .slide: class="slide" -->

- Être capable de facilement s'interfacer avec n'importe quelle autre application

---

## Reproductibilité

<!-- .slide: class="slide" -->

- Être capable de reproduire facilement un scénario ou une situation

---

## Opensource

<!-- .slide: class="slide" -->

- Application avec un code source accessible à libre redistribution possédant une définition du devenir du code partagé

---

## Automatisation

<!-- .slide: class="slide" -->

- Automatiser les processus à faible valeurs ajoutée dans le but de se focaliser sur les tâches les plus intéressantes.

---

## Comment on s'y prends ?

<!-- .slide: class="slide" -->

- Présentation
- Tp avec Utilisation de VScode et liveshare pour coder a plusieurs [liveshare](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare-pack)
