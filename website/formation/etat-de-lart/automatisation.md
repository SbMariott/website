# CI-CD Mise en pratique

<!-- .slide: class="slide" -->

---

## Questions :

<!-- .slide: class="slide" -->

- Qui pense faire du continous integration ?
- Qui pense faire du continous delivery ?
- Qui pense faire du continous deployment ?

---

## Continuous integration (CI)

<!-- .slide: class="slide" -->

Automatiser les tâches autour d'une application :

- Packaging
- Tests, génération de rapport de tests
- Analyse de code (SonarQube)
- Analyse de dépendances (Dependabot)
- Code coverage (Coveralls)

---

## (CD) : Continuous delivery et continous deployment

<!-- .slide: class="slide" -->

Automatiser le dépôt d'un livrable :

- Dépôt d'un livrable (jar, war, build, docker)

Automatiser le déploiement d'un environnement :

- Dépôt d'un livrable sur une machine
- Demande de déploiement d'une application (majiba-api)
- Déploiement d'un environnement à la volée

---

![conteneur-image](img/ci-cd-flow-desktop_1.png)

---

## Intêret

<!-- .slide: class="slide" -->

![siamoi](img/siamoi.png)

- Gain de temps
- Centralisation des process
- Limiter les opérations manuelles

---

## Les solutions de CI

<!-- .slide: class="slide" -->

- GitlabCI (Gitlab)
- Pipeline Jenkins
- travisCI et github-actions (Github)
- Tekton
- etc.

---

### Gitlab-ci (1/2)

<!-- .slide: class="slide" -->

- Les stages tournent les uns à la suite des autres
- Les jobs au sein d'un même stage tournent en parrallèle

---

```yaml
stages:
  - stage1
  - stage2

variables:
  MA_VAR_GLOBALE: toto

job1:
  variable:
    - MA_VAR_LOCALE: tata
  stage: stage1
  script:
    - ma commande....
  tags:
    - maven-jdk11

job2:
  variable:
    - MA_VAR_LOCALE: tata
  stage: stage1
  script:
    - ma commande....
  image: imageName:version
```

---

## Des outils annexes

<!-- .slide: class="slide" -->

Des outils annexes sont mis à disposition afin de faciliter le développement ou le déploiement d'applications

---

### Sonar

<!-- .slide: class="slide" -->

➡️ Un outil opensource dont l'objectif est de réduire la dette technique

---

#### Qu'est-ce que ça fait

<!-- .slide: class="slide" -->

- Analyse statique du code
- Analyse du code sur une branche
- Lors d’une Merge Request calcul de la dette technique
- Génération de rapports et dialogues lors d’une Merge Request

---

#### Qu'est-ce que ça m'apporte ?

<!-- .slide: class="slide" -->

- Qualité du code augmentée
- Aide à la veille technique
- Qualité globale augmentée (performance + stabilité)
- On a une instance en prod : [sonar.insee.fr](sonar.insee.fr)

---

#### Comment je fais ?

<!-- .slide: class="slide" -->

voir [ici](./pdf/Dependabot&Sonar.pdf)

---

### Dependabot

<!-- .slide: class="slide" -->

➡️ Un outil opensource (proposé par github) dont l'objectif est de maintenir à jour les dépendances d'un projet

---

#### Mise en place (1/2)

<!-- .slide: class="slide" -->

- Se créér un token github avec la permission repo
- Ajouter la variable GITHUB_ACCESS_TOKEN en variable ci (Settings -> CI/CD -> Variables)
- Ajouter l'utilisateur dépendabot au projet
- `git checkout -b dependabot-analyze`
- Faire le ménage dans le dépôt ne laisser que le gitlab-ci.yml
- Vider le gitlab-ci.yml et y mettre le code de la prochaine slide
- Pusher
- Créer un schedules sur la branche dependabot-analyze

---

#### Mise en place (2/2)

<!-- .slide: class="slide" -->

```yaml
stages:
  - dependabot

variables:
  GIT_SSL_NO_VERIFY: 1

dependabot:
  image: harbor.developpement.insee.fr/ci-images/dependabot
  stage: dependabot
  variables:
    PACKAGE_MANAGER: maven # Type de package manager
    GITLAB_BRANCH: master # Branche à analyser
    MAIL: mail@example.fr # Mail qui recevra le rapport détaillé
    PROJECT_PATH: path/to/project # outils-transverses/services-dev/majiba-api par exemple
    CREATE_MERGE_REQUEST: "true" # Si pas de merge request enlevé la clé
  script:
    - cd /home/dependabot/dependabot-script
    - bundle exec ruby ./generic-update-script.rb --verbose
  cache:
    paths:
      - vendor/
  only:
    - schedules
  artifacts:
    expose_as: "Rapport format txt"
    expire_in: 1 week
    paths:
      - /home/dependabot/dependabot-script/update-dependencies.txt
```

---

### Nexus

<!-- .slide: class="slide" -->

- Source unique de vérité pour tous vos composants, binaires et artefacts de build
- Distribution efficace des composants et des conteneurs aux développeurs

---

<!-- .slide: class="slide" -->

![conteneur-image](img/nexus.png)

---

### Majiba-api

<!-- .slide: class="slide" -->

- Un outil fait par l'Insee pour l'Insee (loin des standards)
- Demander le déploiement d'un war sur une des plateformes du CEI

---

### Harbor

<!-- .slide: class="slide" -->

- Stocker les images Docker

---

### Pages

<!-- .slide: class="slide" -->

- Un outil GitLab qui met à disposition un serveur permettant d'y déposer des fichiers statiques

---

### Un cluster k8s

<!-- .slide: class="slide" -->

- Pour déployer des environnements à la volée

---

## Mise en pratique (le boss final)

<!-- .slide: class="slide" -->

- Packager l'appli ✔️
- Lancer les tests
- Création image Docker et dépôt sur harbor ✔️
- Dépôt du war sur tomcat
- Utilisation de Sonar
- Dépôt du livrable sur nexus
- (Interfacage Majiba-api)
- Déploiement d'un environnement pour chaque branche
- Embarquer son dépendabot
- Pages

---

### Un client keycloak par environnement

<!-- .slide: class="slide" -->

Problème: Les valid-redirect-uri

Solutions:

- faire une demande au dot a chaque environnement a la volée
- mettre un pattern général: https://toto-\*\*\*
- créer un client a chaque nouvel environnement et supprimer le client lors du kill de l'environnement

---

Un fichier json avec:

```json
{
  "enabled": true,
  "redirectUris": ["https://test.insee.fr/*", "https://newUri.kl/*"],
  "webOrigins": ["+"],
  "bearerOnly": false,
  "consentRequired": false,
  "standardFlowEnabled": true,
  "implicitFlowEnabled": false,
  "directAccessGrantsEnabled": false,
  "serviceAccountsEnabled": false,
  "publicClient": true,
  "protocol": "openid-connect",
  "fullScopeAllowed": true
}
```

---

```yaml
## Creation d'un client
creation-client-kc:
  tags:
    - maven-jdk11
  script:
    - 'RESPONSE=$(curl https://URL_KEYCLOAK/auth/realms/NOM_DU_REALM/clients-registrations/default -X POST -H "Content-Type: application/json" -H "Authorization: Bearer ${KC_QF__INSEE_SSP_REGISTRATION_TOKEN}" --data @client-keycloak.json)'
    - "REG_ACCESS_TOKEN=$(echo $RESPONSE | jq -r .registrationAccessToken)"
    - "CLIENT_ID=$(echo $RESPONSE | jq -r .clientId)"
    - echo "REG_ACCESS_TOKEN=$REG_ACCESS_TOKEN" >> deploy.env
    - echo "CLIENT_ID=$CLIENT_ID" >> deploy.env
    - cat deploy.env
  artifacts:
    reports:
      dotenv: deploy.env
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://MON_APP-$CI_COMMIT_REF_SLUG.kube.insee.fr
    on_stop: stop_review

stop_review:
  stage: deploy
  tags:
    - maven-jdk11
  script:
    - echo $REG_ACCESS_TOKEN
    - echo $CLIENT_ID
    - 'curl -X DELETE https://URL_KEYCLOAK/auth/realms/NOM_DU_REALM/clients-registrations/default/$CLIENT_ID -H "Authorization: Bearer ${REG_ACCESS_TOKEN}"'
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
```

---

### Deployer son chart sur gitlab pages

<!-- .slide: class="slide" -->

```yaml
lint Helm Charts:
  image:
    name: alpine/helm
    entrypoint: ["/bin/sh", "-c"]
  stage: test
  tags:
    - poc-kube
  script:
    - helm lint charts/*

pages:
  image:
    name: alpine/helm
    entrypoint: ["/bin/sh", "-c"]
  stage: deploy
  tags:
    - poc-kube
  script:
    - mkdir -p ./public
    - "echo \"User-Agent: *\nDisallow: /\" > ./public/robots.txt"
    - helm package charts/* --destination ./public
    - helm repo index --url https://${CI_PROJECT_NAMESPACE}.gitlab-pages.insee.fr/${CI_PROJECT_NAME} .
    - mkdir -p ./public/public
    - mv public/*.tgz ./public/public
    - mv index.yaml ./public
  artifacts:
    paths:
      - public
  only:
    - master
```

---

### Mise en place d'un workflow

<!-- .slide: class="slide" -->

- Optimiser son CI/CD
- Déployer des environnements oui, mais savoir lequel déployer c'est mieux.

➡️ Au DOT :

- Une merge request = un environnement à la volée dans Kubernetes
- Le code sur master = code en dev
- Un tag déploie le code en qf, puis peut déployer le code en pp et en prod

---

### Et si on allait encore plus loin ?

<!-- .slide: class="slide" -->

- Distinction entre dépôt de code (de dev) et dépôt servant à décrire la conf (ops) de notre application (Road to prod)

---

### DevOps ?

<!-- .slide: class="slide" -->

- L’équipe de développement logiciel ou applicatif se charge de collecter les besoins métiers et de les développer. Elle teste ensuite le logiciel ou l’application lorsqu’elle est finalisée.
- Si le logiciel ou l’application répond aux besoins métiers, le code source est mis à disposition de l’équipe opérationnelle pour la partie exploitation.

➡️ Le dev: mais que fait la prod ? ça fait 5 jours que je leur ai demandé de déployer mon appli

➡️ l'ops: Non mais attends, c'est quoi cette appli ? elle tourne pas

---

<!-- .slide: class="slide" -->

➡️ C'est une manière de travailler qui rapproche développeur et opérateur. Cela nécessite :

- Forte communication
- Collaboration entre dev et ops
- Partage du savoir entre dev et ops
- Passage des équipes techniques a des équipes produits

![conteneur-image](img/devops-whatisdevops.png)

---

### Avantage DevOps

<!-- .slide: class="slide" -->

- Casser les silos : démystification du travail de chacun
- Des livraisons/intégrations plus rapides et plus sures
- Meilleure collaboration
- Meilleure sécurité : Infrastructure as code + automatisation des process
- Meilleure gestion des risques : La prod a une vision sur le code du dev et inversement

---

### GitOps

<!-- .slide: class="slide" -->

- Evolution du DevOps
- Tout passe par Git
- Automatisation du SI as code

---

### Argo-cd (1/3)

<!-- .slide: class="slide" -->

- Un outil opensource qui répond au besoin GitOps
- Fonctionne dans k8s
- Surveille un dépôt contenant la configuration de l'application et fait tendre l'état de l'application du cluster vers l'état du dépôt Git

  ![conteneur-image](img/argo.png)

---

### Argo-cd(2/3) ?

<!-- .slide: class="slide" -->

A quoi ça me sert ?

- Décrire des plateformes (équivalentes aux plateformes de qf directement sur Git) --> donc des plateformes qui vivent longtemps
- Le dev a accès aux logs et métriques de son appli

---

### Argo-cd(3/3) ?

<!-- .slide: class="slide" -->

- Petite démo
