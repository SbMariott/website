# Kubernetes en pratique

<!-- .slide: class="slide" -->

---

## Un peu d'archi

<!-- .slide: class="slide" -->

![orchestrateur](img/kubernetes.png)

✔️ Objectif communiquer avec l'api-server

---

## Installer kubectl sur son poste

<!-- .slide: class="slide" -->

- [ici](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/)
- Ajouter l'exécutable au path

---

## Faire fonctionner kubectl sur poste insee

<!-- .slide: class="slide" -->

- Avoir défini les variables d'environnement http_proxy et https_proxy et no proxy

---

## Récupérer mes credentials

<!-- .slide: class="slide" -->

- [welcome.kube.developpement.insee.fr](https://welcome.kube.developpement.insee.fr)

✔️ Creation d'un fichier dans `$HOME/.kube/config` avec toutes les informations pour authentification

---

## Quelques outils supplémentaires

<!-- .slide: class="slide" -->

- Plugin VSCode
- Outils lens

---

### TP (1/2)

<!-- .slide: class="slide" -->

- Installer kubectl sur votre poste
- Récupérer vos credentials pour l'interne (resp en externe) sur [welcome.kube.developpement.insee.fr](https://welcome.kube.developpement.insee.fr) (resp [dev.insee.io](https://dev.insee.io))
- VS code installation de l'extension kubernetes
- Jouer avec kubectl (`kubectl get pods`)

---

## Namespace

<!-- .slide: class="slide" -->

- Partition
  virtuelle d’un cluster Kubernetes associée
  à un groupe d’utilisateur
- Totalement isolé des autres namespaces

✔️ Je fais (presque) ce que je veux dedans

---

## Mes premiers objets K8S

<!-- .slide: class="slide" -->

- Utilisation de fichier yaml (Yet Another Markup Language)
- Déployer une appli = plusieurs briques assemblées
- Plus de 150 types de ressources de bases

---

### Deployments

<!-- .slide: class="slide" -->

- Création d'un pod (plus petite entité)
- Choix de l'image à utiliser
- Choix des ports du conteneur à exposer
- Demande de stockages
- Choix du nombre de CPU et de la mémoire alloués
- choix du nombre de réplicas

✔️ L'appli tourne dans le cluster mais n'est exposé à personne pour le moment.

---

### Service

<!-- .slide: class="slide" -->

- Exposer l'application à l'intérieur du cluster
- Accéder à un service d'un autre namespace : serviceX.namespaceB.svc.cluster.local

✔️ L'appli tourne dans le cluster et est accessible à toute personne dans le cluster.

---

### Ingress

<!-- .slide: class="slide" -->

- Exposer l'application au monde entier
- Choix de l'url

✔️ L'appli tourne dans le cluster et est accessible au monde entier.

---

### Autres objets

<!-- .slide: class="slide" -->

- ConfigMap
- Secret

---

## Quelques commandes utiles

<!-- .slide: class="slide" -->

---

### Création d'objets

<!-- .slide: class="slide" -->

```bash

kubectl apply -f ./my-manifest.yaml            # Crée une ou plusieurs ressources
kubectl apply -f ./my1.yaml -f ./my2.yaml      # Crée depuis plusieurs fichiers
kubectl apply -f ./dir                         # Crée une ou plusieurs ressources depuis tous les manifests dans dir
kubectl create deployment nginx --image=nginx  # Démarre une instance unique de nginx
kubectl explain pods                           # Affiche la documentation pour les manifests pod
kubectl replace --force -f ./pod.json          # Remplace de manière forcée (Force replace), supprime puis re-crée la ressource. Provoque une interruption de service.

```

---

### Lister les objets

<!-- .slide: class="slide" -->

```bash
# Commandes Get avec un affichage basique
kubectl get services                     # Liste tous les services d'un namespace
kubectl get pods --all-namespaces        # Liste tous les Pods de tous les namespaces
kubectl get pods -o wide                 # Liste tous les Pods du namespace courant, avec plus de détails
kubectl get deployment my-dep            # Liste un déploiement particulier
kubectl get pods                         # Liste tous les Pods dans un namespace
kubectl get pod my-pod -o yaml           # Affiche le YAML du Pod

# Commandes Describe avec un affichage verbeux
kubectl describe nodes my-node
kubectl describe pods my-pod

```

---

### Interagir avec des pods en cours d'exécution

<!-- .slide: class="slide" -->

```bash
kubectl run -i --tty busybox --image=busybox -- sh  # Exécute un pod comme un shell interactif
kubectl logs my-pod                                 # Affiche les logs du pod (stdout)
kubectl port-forward my-pod 5000:6000               # Écoute le port 5000 de la machine locale et forwarde vers le port 6000 de my-pod
kubectl top pod POD_NAME --containers               # Affiche les métriques pour un pod donné et ses conteneurs
```

---

## TP (2/2)

<!-- .slide: class="slide" -->

- exercice 1 : Réaliser le fichier deployments.yaml associé à l'application opensource
- exercice 2 : S'assurer que l'application tourne bien en port-forwardant le pod et/ou en récuperant ses logs
- exercice 3 : Créer les couches supérieures : ingress + service
- exercice 4 : Faire la même chose avec l'application Insee. Ajouter les properties qui vont bien pour intégrer avec l'environnement Insee (keycloak par exemple)

---

### Se créer un compte de service (Pipeline)

<!-- .slide: class="slide" -->

- Pourquoi faire ? Car vos jetons ont une durée de vie limité.
- Comment ?

```bash
kubectl create serviceaccount service_account_name
kubectl create rolebinding serviceaccounts-edit --clusterrole=admin --serviceaccount=le_namespace:service_account_name
kubectl get serviceaccount service_account_name -o=jsonpath='{.secrets[0].name}' | xargs kubectl get secret -o=jsonpath='{.data.token}' | base64 --decode
```

---

## Quelques remarques :

- On a fait du déploiement as-code
- Pour différentes applis les contrats sont presques les mêmes

---

## Quelques outils mis à disposition (INSEE)

<!-- .slide: class="slide" -->

- grafana
- argocd
- istio

---

### Grafana

<!-- .slide: class="slide" -->

Couplé à Prometheus, Grafana expose des dashboards permettant de récuperer des métriques du cluster. [http://grafana.kube.developpement.insee.fr](http://grafana.kube.developpement.insee.fr)

✔️ Monitoring
✔️ Traçabilité
✔️ Disponibilité

---

### Kiali

<!-- .slide: class="slide" -->

Permet de surveiller les flux réseaux (entrant + sortant) au sein du cluster
[https://kiali.kube.developpement.insee.fr/kiali](https://kiali.kube.developpement.insee.fr/kiali)

✔️ Traçabilité

---

### Kubernetes dashboard

<!-- .slide: class="slide" -->

➡️ Fournir une ui à kubectl

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended.yaml && kubectl proxy
```

✔️ Monitoring

---

### ArgoCD

<!-- .slide: class="slide" -->

Permet de déployer des applications en mode GitOps (Voir partie suivante)

---

## Helm

<!-- .slide: class="slide" -->

---

## Helm: comment ça marche ?

<!-- .slide: class="slide" -->

![helm](img/helm.drawio.png)

---

<!-- .slide: class="slide" -->

![helm](img/helm2.drawio.png)

---

<!-- .slide: class="slide" -->

- Un gestionnaire de paquet pour Kubernetes
- Une partie des paquets dispo [ici](https://hub.kubeapps.com/)

---

## Installation de helm

<!-- .slide: class="slide" -->

[https://helm.sh/docs/intro/install/](https://helm.sh/docs/intro/install/)

- Des charts qui sont des manifests templatisés avec un fichier values qui vient remplir les trous
- On peut override toutes les values

---

## Quelques commandes helm

<!-- .slide: class="slide" -->

[https://helm.sh/docs/intro/using_helm/](https://helm.sh/docs/intro/using_helm/)

---

## Déployer avec helm ?

<!-- .slide: class="slide" -->

Démo

---

## Need hel(p)m

<!-- .slide: class="slide" -->

- Partager une application
- Mettre a disposition des environnements de formations ([#onyxia](https://datalab.sspcloud.fr/home))
- Aller en production

---

## Créèr mon chart helm

<!-- .slide: class="slide" -->

```
helm create monApp
```
