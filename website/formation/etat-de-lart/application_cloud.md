# Application cloud-native

<!-- .slide: class="slide" -->

---

## Test :

Ces applications sont-elles cloud-natives ?

<!-- .slide: class="slide" -->

- Une application monolithique
- Une application qui tourne dans un conteneur
- Une application qui stocke des données en session
- Une application 3 couloirs avec affinité
- Une application n couloirs sans affinité
- Une application respectant l'architecture micro-service, stateless, scalable horizontalement et verticalement.

---

## Ce n'est pas :

<!-- .slide: class="slide" -->

⚠️ Une application qui tourne dans un conteneur

---

## C'est quoi une application CloudNative?

<!-- .slide: class="slide" -->

```
"Les technologies cloud-native permettent aux entreprises de construire et d’exploiter des applications
élastiques dans des environnements modernes et dynamiques comme des clouds publics,
privés ou bien hybrides. Les conteneurs, les services mesh, les micro services, les infrastructures
immuables et les API REST illustrent cette approche. Ces techniques permettent la mise en oeuvre
de systèmes faiblement couplés, à la fois résistants, pilotables et observables. Combinés à un
robuste système d’automatisation, ils permettent aux ingénieurs de procéder à des modifications
impactantes, fréquemment et de façon prévisible avec un minimum de travail."
```

Définition de la [CNCF](https://www.cncf.io/) 😅

---

<!-- .slide: class="slide" -->

- Scalabilité verticale : augmentation des ressources allouées : CPU, mémoire...
- Scalabilité horizontale : augmentation ou diminution du nombre de réplication
- Stateless : Application sans état (qui ne stocke aucune donnée en session)

---

## Conclusion

<!-- .slide: class="slide" -->

- Oui mais mon appli n'est pas cloudnative, tout ce que tu viens de raconter ça ne me sert à rien ? 😭
- Même si mon appli n'est pas cloud native, elle peut tourner dans un cluster Kubernetes, mais elle ne sera juste pas optimisée pour !!! <!-- .element: class="fragment fade-in visible" -->
