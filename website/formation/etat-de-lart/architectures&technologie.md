# Architecture & technologie

<!-- .slide: class="slide" -->

---

## Un peu d'histoire

<!-- .slide: class="slide" -->

- Mainframe
- Serveurs dédiés
- Machines virtuelles
- Conteneurisation
- Orchestration de conteneurs

---

## MainFrame (Ordinateur central)

<!-- .slide: class="slide" -->

- La plus ancienne des technologies citées à l'origine de l'architecture client-serveur.
- Haute performance, haute sécurité et haute disponibilité
- C'est vieux, c'est du cobol
- Mort depuis les années 95 mais toujours présent dans certains grands groupes (notamment les banques)

---

## Serveurs dédiés (Bare metal server)

<!-- .slide: class="slide" -->

![Bare Metal Serveur](img/baremetal.drawio.png)

- Forte isolation, très bonne performance, cher

---

## Machine virtuelle

<!-- .slide: class="slide" -->

![Machine virtuelle](img/virtualmachine.drawio.png)

- Forte isolation, performance moyenne, pas cher.

---

## Conteneur

<!-- .slide: class="slide" -->

![Conteneur](img/conteneur.drawio.png)

- Isolation moyenne, bonne (voir très bonne) performance, pas cher. Réponds au besoin de la scalabilité et du cloud native.

---

## VM vs conteneur

<!-- .slide: class="slide" -->

![Conteneur](img/vm_vs_container.drawio.png)

---

## Orchestrateur de conteneurs

<!-- .slide: class="slide" -->

- Industrialiser la conteneurisation

---

## Outils d'installation

<!-- .slide: class="slide" -->

### A l'INSEE (les anciens)

- Puppet (Description d'un état souhaité) ([control-repo](https://gitlab.insee.fr/cigal/puppet/control-repo))
- Salt
- Stack insee (Crapo, etc...)

### En cours d'introduction

- Ansible (Ensemble d'opération à effectuer pour arriver à un état souhaité)
