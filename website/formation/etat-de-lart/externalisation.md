# Externaliser la configuration

<!-- .slide: class="slide" -->

---

## C'est quoi la configuration ?

<!-- .slide: class="slide" -->

- Les ressources gérées par la base de données, Memcached, ou tout autre service de stockage
- Les identifiants pour des services externes, tel qu’Amazon S3 ou Twitter
- Les Uris par défaut
- Les valeurs spécifiques au déploiement, tel que son nom d’hôte canonique

⚠️ tout ce qui est susceptible de varier entre des déploiements

---

## Ce qui n'est pas de la configuration

<!-- .slide: class="slide" -->

- paramètres internes de l’application

---

## Ai-je bien externaliser la configuration ?

<!-- .slide: class="slide" -->

- Mon appli peut-elle devenir opensource à tout moment ?

---

## Springboot, la solution ?

<!-- .slide: class="slide" -->

- Chargement depuis les fichiers properties (une property par "profil")
- Chargement depuis les properties données dans la ligne de commande

```
java -jar app.jar --property="value"
```

- Chargement depuis les properties données par system properties

```
java -Dproperty.name="value" -jar app.jar
```

- Chargement depuis les variables d'environnement

---

## TP

<!-- .slide: class="slide" -->

Identifier et extraire tout ce qui est de la configuration en properties
