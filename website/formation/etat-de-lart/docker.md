# Docker

<!-- .slide: class="slide" -->

---

## Un peu de vocabulaire :

<!-- .slide: class="slide" -->

- Conteneur
- DockerHub
- Image Docker
- DockerFile
- Docker engine, Docker compose, Docker swarm,...

---

## Installation de Docker

<!-- .slide: class="slide" -->

- [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)
- Sur les postes Insee c'est galère (poste admin + wsl)

---

## Les commandes de bases

<!-- .slide: class="slide" -->

`docker run hello-world`

`docker run -it ubuntu`

`docker run -it ubuntu /bin/bash`

`docker run -p 8888:80 nginx`

`docker run --env POSTGRES_PASSWORD=secret -p 5432:5432 postgres:13`

`docker run --env POSTGRES_PASSWORD=secret -p 5432:5432 -d postgres:13`

---

## Management (local) de conteneurs

<!-- .slide: class="slide" -->

`docker ps`

`docker logs ID`

`docker kill ID`

`docker exec -it ID COMMAND`

---

## Créer une image (Management d'image)

<!-- .slide: class="slide" -->

Dockerfile :

```
FROM ubuntu
RUN apt-get update
RUN apt-get -y install cowsay
ENTRYPOINT ["/usr/games/cowsay"]
```

Docker build :

`docker build -t toto .`

Lancer mon image :

`docker run toto`

---

## Publier mon image

<!-- .slide: class="slide" -->

- Partager son image

---

### Dans le monde opensource

<!-- .slide: class="slide" -->

- Utilisation de DockerHub (qui est un dépôt public)
- Obligation d'avoir des crédentials

```bash
docker build --pull -t "donatien26/appli-opensource:mon_tag" .  # On build l'image
docker login -u __username__ -p __password__    # On s'authentifie sur dockerhub
docker push "donatien26/appli-opensource:mon_tag"   # On push sur dockerhub
```

ici on utilise Docker, mais le login/build/push sont des opérations gérées par la CLI Docker mais pas spécifique à Docker.

---

### A l'Insee

<!-- .slide: class="slide" -->

---

## Harbor (Registry interne)

<!-- .slide: class="slide" -->

- Utilisation de kaniko
- Création d'un dépôt sur harbor.developpement.insee.fr (équivalent git-registry sur innovation)

```
- mkdir -p /kaniko/.docker
- echo "{\"auths\":{\"harbor.developpement.insee.fr\":{\"username\":\"$REGISTRY_USER_WITH_DOLLAR\",\"password\":\"$REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
- /kaniko/executor --context . --dockerfile Dockerfile --destination mon-registry.example.com/depot/appli-opensource:mon_tag --skip-tls-verify

```

---

## TP

<!-- .slide: class="slide" -->

- Utilisation de [https://labs.play-with-docker.com/](https://labs.play-with-docker.com/)
- exercice 1 : Lancer l'image de votre choix sur votre poste
- exercice 2 : Lancer l'image nginx en exposant le port 80 sur votre machine,
- modifier le fichier index.html.
- exercice 3 : Créer l'image Docker associée à l'application opensource (attention ici on a un JAR)
- exercice 4 : Faire le pipeline pour la publier sur dockerhub
- exercice 5 : Créer l'image docker associée à l'application insee (attention ici on a un WAR)
- exercice 6 : Faire le pipeline pour la publier sur harbor

---

## Quelques autres utilités du conteneur:

<!-- .slide: class="slide" -->

- Travailler dans un conteneur [https://code.visualstudio.com/docs/remote/containers](https://code.visualstudio.com/docs/remote/containers)
- Mise en place d'environnement isolé
